import React from 'react';
import './App.less';
import './App.css';
import { Authentication } from './auth/Authentication';
import AppLayout from './layouts/AppLayout';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LobbyPage from './page/LobbyPage';

const App = () => {
  return (
    <BrowserRouter>
      <Authentication>
        <Switch>
          <Route exact path="/lbp" component={LobbyPage} />
          <Route path="/" component={AppLayout} />
        </Switch>
      </Authentication>
    </BrowserRouter>
  )
}

export default App;