import React, { useContext, useEffect, useState } from 'react'
import firebase from 'firebase'
import { useHistory } from 'react-router-dom'

export const Context = React.createContext()

export const Authentication = ({ children }) => {
    const [user, setuser] = useState(null)
    const history = useHistory()
    useEffect(() => {
        firebase.auth().onAuthStateChanged(setuser)
    }, [])


    const login = (_user, _pass) => {
        // console.log("🚀 ~ file: Authentication.js ~ line 6 ~ login ~ _user", _user, _pass)
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const logedIn = firebase.auth().signInWithEmailAndPassword(_user, _pass)
            .then((_user) => {
                console.log("🚀 ~ file: Authentication.js ~ line 27 ~ .then ~ _user", _user)
                window.localStorage.setItem('uid', _user.user.uid)
                return _user
            })
            .catch((_error) => {
                console.log('firebase _e : ', _error.message)
            })
        if (!!logedIn) {
            history.push('/')
        }
        console.log("🚀 ~ file: Authentication.js ~ line 26 ~ login ~ logedIn", logedIn)
        return logedIn

    }

    return (
        <Context.Provider value={{ user, login }}>
            {children}
        </Context.Provider>
    )
}

export const useAuth = () => {
    const context = useContext(Context)
    // console.log("🚀 ~ file: Authentication.js ~ line 36 ~ useAuth ~ context", context)
    if (!context) {
        throw new Error('useAuth should be use inside AuthContext')
    }
    return context
}

export const useAuthenticated = () => {
    const history = useHistory()
    const { user } = useAuth()
    if (!user) {
        // console.log("🚀 ~ file: Authentication.js ~ line 72 ~ useAuthenticated ~ user", user)
        history.push('/lbp')
    }
}

export default {
    // google,
    // facebook,
    // register,
    // login,
    // useAuthenticated,
    // sendResetPassword,
    // confirmResetPassword,
    Authentication
}
