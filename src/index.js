import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBHdoyVPc4zWo__wqdfYU9li0HGvvNz5mg",
  authDomain: "covid-esc.firebaseapp.com",
  projectId: "covid-esc",
  storageBucket: "covid-esc.appspot.com",
  messagingSenderId: "616959113131",
  appId: "1:616959113131:web:2f54e345073827de2204ee",
  measurementId: "G-SWRVZFCWWF"
}
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
