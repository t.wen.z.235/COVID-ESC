import React from 'react'
import { Route, Switch } from 'react-router-dom'
import MainPage from '../page/MainPage'
import UserPage from '../page/UserPage'

const AppContent = () => {
    console.log('app content')
    return (
        <Switch>
            <Route exact path='/' component={MainPage} />
            <Route path='/user' component={UserPage} />
        </Switch>
    )
}

export default AppContent
