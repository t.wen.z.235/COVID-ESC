import { Col, Menu, Row } from 'antd'
import React from 'react'
import { useHistory } from 'react-router-dom'


const AppHeader = () => {
    console.log('header')
    const history = useHistory()
    const _logout = () => {
        window.localStorage.clear()
        window.indexedDB.deleteDatabase('firebaseLocalStorageDb')
        window.location.reload()
    }
    return (
        <Row justify='space-between'>
            <Col style={{ paddingLeft: '5%' }}>
                <Menu mode='horizontal' >

                    <Menu.Item key='1' onClick={() => history.push('/')}>
                        main
                    </Menu.Item>
                    <Menu.Item key='2' onClick={() => history.push('/user')}>
                        user
                    </Menu.Item>
                </Menu>
            </Col>
            <Col>
                <Menu mode='horizontal' >
                    <Menu.Item onClick={_logout}>
                        logout
                    </Menu.Item>
                </Menu>
            </Col>
        </Row>

    )
}

export default AppHeader
