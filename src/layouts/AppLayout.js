import { Layout } from 'antd'
import React from 'react'
import { useAuthenticated } from '../auth/Authentication'
import AppContent from './AppContent'
import AppHeader from './AppHeader'

const AppLayout = () => {
    useAuthenticated()
    console.log('app layout')
    return (
        <Layout>
            <Layout.Header>
                <AppHeader />
            </Layout.Header>
            <Layout.Content>
                <AppContent />
            </Layout.Content>
        </Layout>
    )
}

export default AppLayout