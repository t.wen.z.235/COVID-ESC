import { Button, Input, Row } from 'antd'
import Form from 'antd/lib/form/Form'
import FormItem from 'antd/lib/form/FormItem'
import React from 'react'
import { useAuth } from '../auth/Authentication'

const LobbyPage = () => {
    const { login } = useAuth()
    const _login = async (params) => {
        try {
            await login('thebasic555@gmail.com', 'Tn@02052535')
        } catch (error) {
            console.log('error : ', error)
        }
    }
    return (
        <>
            <Row style={{ height: '40%', backgroundColor: 'red' }}>
                asdf
            </Row>
            <Row style={{ height: '40%', backgroundColor: 'royalblue' }}>
                <Form onFinish={_login}>
                    <FormItem
                        label="Username"
                        name="username"
                        rules={[{ required: true, message: 'Please input your username!' }]}>
                        <Input placeholder={'username'} />
                    </FormItem>
                    <FormItem
                        label="Password"
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}>
                        <Input placeholder={'password'} />
                    </FormItem>
                    <FormItem>
                        <Button type="primary" htmlType="submit">Submit</Button>
                    </FormItem>
                </Form>
            </Row>
            <Row style={{ height: '40%', backgroundColor: 'rosybrown' }}>
                zcc
            </Row>
        </>
    )
}

export default LobbyPage
