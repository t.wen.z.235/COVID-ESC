import { Loader } from '@googlemaps/js-api-loader';
import { Row } from 'antd'
import React from 'react'

const MainPage = () => {
    console.log('main')
    const callMap = () => {
        const loader = new Loader({
            apiKey: "AIzaSyBblVluu3KKeDL37YL3YHBOSW0Hgb-3cXc",
            version: "weekly"
        });
        loader.load().then(() => {
            // eslint-disable-next-line no-undef
            new google.maps.Map(document.getElementById("map"), {
                center: { lat: -34.397, lng: 150.644 },
                zoom: 8,
            });
        });
    }
    // callMap()

    return (
        <>
            <Row style={{ height: '400px', width: '400px', backgroundColor: 'royalblue' }}>
                <div id="map">
                    Main
                </div>
            </Row>
        </>
    )
}

export default MainPage
